#!/bin/bash
# Generates OpenVPN .ccd file
source ./config.sh

echo -n > openvpn.ccd
while read line
do
	echo "push \"route $line\"" >> openvpn.ccd
done < iplist_collapsed_mask.txt

cp ./openvpn.ccd "${OVPNCCDPH}"
