# Skipping empty strings
(!$1) {next}

{d_ip[$1] = $1}

function printarray(arrname, arr) {
    d_printed_end = 0
    print arrname, "= \"\\"
    for (i in arr) {
        d_printed_end = 0
        printf arr[i] " "
        if (i % 32 == 0) {
            print "\\"
            d_printed_end = 1
        }
    }
    if (d_printed_end == 0) {
        print "\\"
    }
    print "\".split(\" \");"
    print ""
}

# Final function
END {
    asort(d_ip)

    printarray("d_ipaddr", d_ip)
}

