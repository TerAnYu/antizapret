#!/bin/bash
source ./config.sh

echo -n > proxy-host.pac

# .pac header
echo "// ProstoVPN.AntiZapret PAC-host File
// Generated on $(date)
" >> proxy-host.pac

#awk -f domainoptim_string.awk hostlist_nocustomhosts.txt >> proxy-host.pac
awk -f domainoptim_string.awk hostlist.txt >> proxy-host.pac

awk -f ipoptim_string.awk customips.txt iplist_blockedbyip.txt >> proxy-host.pac

echo "
function FindProxyForURL(url, host) {" >> proxy-host.pac

echo "  if (/\.(ru|co|cu|com|info|net|org|gov|edu|int|mil|biz|pp|ne|msk|spb|nnov|od|in|ho|cc|dn|i|tut|v|dp|sl|ddns|duckdns|livejournal|herokuapp|azurewebsites|ucoz|3dn|nov)\.[^.]+$/.test(host))
    host = host.replace(/(.+)\.([^.]+\.[^.]+\.[^.]+$)/, \"\$2\");
  else
    host = host.replace(/(.+)\.([^.]+\.[^.]+$)/, \"\$2\");

  if (/^[a-d]/.test(host)) curarr = d_ad;
  else if (/^[e-h]/.test(host)) curarr = d_eh;
  else if (/^[i-l]/.test(host)) curarr = d_il;
  else if (/^[m-p]/.test(host)) curarr = d_mp;
  else if (/^[q-t]/.test(host)) curarr = d_qt;
  else if (/^[u-z]/.test(host)) curarr = d_uz;
  else curarr = d_other;

  var oip = dnsResolve(host);

  for (var i = 0; i < d_ipaddr.length; i++)
    if (oip === d_ipaddr[i]) {oip = 1; break;}

  for (var i = 0; i < curarr.length; i++) {
    if (oip === 1 || host === curarr[i]) {" >> proxy-host.pac

cp proxy-host.pac proxy-host-nossl.pac
echo "      return \"PROXY ${PACPROXYHOST}; DIRECT\";" >> proxy-host-nossl.pac
echo "      return \"HTTPS ${PACHTTPSHOST}; PROXY ${PACPROXYHOST}; DIRECT\";" >> proxy-host.pac

echo "    }
  }

  return \"DIRECT\";
}" | tee -a proxy-host.pac proxy-host-nossl.pac >/dev/null

cp ./proxy-host.pac /usr/share/nginx/html/antizapret/
cp ./proxy-host-nossl.pac /usr/share/nginx/html/antizapret/
rm /usr/share/nginx/html/antizapret/proxy-host.pac.gz
rm /usr/share/nginx/html/antizapret/proxy-host-nossl.pac.gz
gzip -k9 /usr/share/nginx/html/antizapret/proxy-host.pac
gzip -k9 /usr/share/nginx/html/antizapret/proxy-host-nossl.pac
