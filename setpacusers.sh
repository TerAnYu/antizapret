#!/bin/bash
source ./config.sh

# Add IP addresses to ipset 'pacusers4' and 'pacusers6' table
echo "Flushing ipset rules"
ipset destroy pacusers4
ipset flush pacusers4
ipset destroy pacusers6
ipset flush pacusers6

# Creating it again
ipset create pacusers4 hash:ip maxelem 262144
ipset create pacusers6 hash:ip family inet6 maxelem 131072

echo "Adding pacusers to ipset"

ipset restore < <(awk '/\/proxy/ {if ($1 !~ /\:/) v4[$1]=$1; else v6[$1]=$1} END {for (i in v4) print "add pacusers4",i; for (i in v6) print "add pacusers6",i;}' "${SLP}"/az.log "${SLP}"/az.log.?)
#ipset -\! restore < <(ssh -C root@antizapret-2.prostovpn.org 'awk '\''/\/proxy\.pac/ {if ($1 !~ /\:/) v4[$1]=$1; else v6[$1]=$1} END {for (i in v4) print "add pacusers4",i; for (i in v6) print "add pacusers6",i;}'\'' /var/log/nginx/az.log /var/log/nginx/az.log.1')
ipset -\! restore < <(ssh -C root@antizapret-2.prostovpn.org 'ipset save pacusers4' | tail -n +2)
ipset -\! restore < <(ssh -C root@antizapret-2.prostovpn.org 'ipset save pacusers6' | tail -n +2)

echo "Done!"
