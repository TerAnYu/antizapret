#!/bin/bash
source ./config.sh

# Generate dnsmasq aliases

echo -n > dnsmasq-aliases.conf

while read line
do
	echo "alias=$line,$DNSPROXYIP" >> dnsmasq-aliases.conf
done < iplist.txt

cp ./dnsmasq-aliases.conf "${DAPF}"
