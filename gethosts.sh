#!/bin/sh

# Extract hosts from list
tail -n +2 list.xml | grep -F -v -f ignoreips.txt | awk -F ';' '{print $2}' | cat - customhosts.txt | sort -u | grep -F -v -f ignorehosts.txt | awk '/^$/ {next} {print}' | idn > hostlist.txt
tail -n +2 list.xml | grep -F -v -f ignoreips.txt | awk -F ';' '{print $2}' | cat - customhosts_hosts.txt | sort -u | grep -F -v -f ignorehosts.txt | awk '/^$/ {next} {print}' | idn > hostlist_nocustomhosts.txt
#cat customhosts.txt | sort -u | grep -v -f ignorehosts.txt > hostlist.txt
echo -n "Unique hosts in list: "
cat hostlist.txt | wc -l
